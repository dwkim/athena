# ATLAS Qualification Tasks - Geant4 Physicslist Optimization

## Athena Release Setup
1. Simulation with default MC16 Run-2 Settings (Geant4 10.1)
 * on lxplus:
 
   ```
 $ setupATLAS
 $ asetup 21.0.120, Athena
 ``` 
 
2. Simulation with Geant4 10.6, but otherwise default MC16 Run-2 Settings
 * on lxplus:

   ```
 $ setupATLAS
 $ asetup local/simulation/21.0,2020-06-08T1530,Athena
 ```

3. Geant4 10.6 (full simulation: HIT > ... > AOD)

 Reference: [Athena 21.0 Nightly Build](https://its.cern.ch/jira/browse/ATLASSIM-5088), 
            [Information about Nightly Builds](https://atlassoftwaredocs.web.cern.ch/athena/athena-nightly-builds/), [Location of Releases](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/AtlasSetup#1_1_Location_of_releases)
            
 * on lxplus:

  ```
 $ setupATLAS
 $ asetup local/simulation/21.0,r2021-02-23T1818,Athena

 ```

 In this nightly build version of Athena, the biggest difference from the previous setup (for Geant4 10.6) is that the entire production ranging from G4 simulation in Athena, Digitisation, and Reconstruction (**EVNT > HIT > RD0/ESD > AOD**) is enabled in the same terminal.

4. Git Setup
 
 * on lxplus:

 ```
 $ setupATLAS
 $ lsetup git
 ```
 
## ROOT Local Setup (M1 Apple Silicon)
Due to the advent of the new apple processor, some of the packages (previously built on the x86 architecture, namely for Intel processors) do not seem to work as of *January 2021*.  Here is the method for installing the **ROOT** in one's local directory using **conda**.

* Download **miniconda** for Apple M1 chip version and download the **ROOT**:

 ```
 $ conda install root
 ```

## ROOT - Useful Commands
* Merge .root files into one

  ```
  $ hadd <output-file-name.root> <input-files.root>

  ```

  (Example)

  ```
  $ hdd ntuple.root *.ntuple.pool.root
  ```

* Open .root files using the ROOT

  ```
  $ root -l <name of the root file>.root
  ```

* After one opens the ROOT, one can find the name of files, trees, and objects using the following command:

  ```
$ .ls
TFile**		ParticleGun_pid22_E20000_eta_020_randomSeed_12341.ntuple.pool.root
 TFile*		ParticleGun_pid22_E20000_eta_020_randomSeed_12341.ntuple.pool.root
  OBJ: TTree	photons	photons : 0 at: 0x33d2230
  KEY: TTree	photons;1	photons
```

* Display the variables for the tree (e.g. "photons")

  ```
$ photons->Print()
*Br   47 :DeltaE    : DeltaE/F                                               *
*Entries :      513 : Total  Size=       2617 bytes  File Size  =        995 *
*Baskets :        1 : Basket Size=      32000 bytes  Compression=   2.14     *
*............................................................................*
*Br   48 :etaCalo   : etaCalo/F                                              *
*Entries :      513 : Total  Size=       2622 bytes  File Size  =       1881 *
*Baskets :        1 : Basket Size=      32000 bytes  Compression=   1.13     *
*............................................................................*
*Br   49 :phiCalo   : phiCalo/F                                              *
*Entries :      513 : Total  Size=       2622 bytes  File Size  =       2033 *
*Baskets :        1 : Basket Size=      32000 bytes  Compression=   1.05
``` 

* Draw the histogram for specific variables or the branch names (e.g. "etaCalo")

  ```
$ photons->Draw("etaCalo")
$ tree->Draw("branch", "cut")
```

* If one wants to peek a specific branch inside the tree

  ```
$ tree->Scan("branch")
```

* If one wants to look inside the leaf of the tree (TTree -> TBranch -> TLeaf), for example, when one opens up the ***.HITS.pool.root in order to look at the information regarding the timing, the following command enables one to draw a histogram with the leaf called "timings[0]" from the tree called "CollectionTree" ( [0] is present due to the fact that the timings is a vector and 0th component contains the runtime for each event in the ---.HITS.pool.root files.)

  ```
$ CollectionTree->Draw("RecoTimingObj_p1_EVNTtoHITS_timings.timings[0]")
```

## uproot
Instead of using .cxx and .py in the ROOT or pyRoot framework, one can use another framework called $\textit{uproot}$, which enables one to use the functionalities of python after implementing the .root files.  The setup can be done creating the account in SWAN, in which one can use jupyter notebook.  If one already has the cernbox installed, one can see the directory linked to the cernbox (i.e. in eos).  The jupyter notebook(.ipynb) is more user-friendly in a sense that one can run individual cell after writing the code.  Here is the schematic picture of the framework:

![uproot_picture](Screenshot 2021-05-05 at 16.04.51.png)

* For the analysis of the photon shower distribution (generated with the Geant4), the plots and histograms are drawn using this program.

## Storage
 
Due to the limited spaces in */afs/cern.ch/user/d/dwkim/qt\_test\_area* (approx. 10 GB), one can store the output files in the */eos/user/d/dwkim* (approx. 1TB), which can be displayed in the local directory **cernbox**.

## Geant4 simulation inside Athena (Basics)
### Output HIT file
1. Steering File to generate photons with E = 20 GeV, $\eta$ = 0.2
 * Information stored in the file name: *ParticleGun.Photons\_E20\_eta020.py*

```python
import AthenaCommon.AtlasUnixGeneratorJob
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()
import ParticleGun as PG
pg = PG.ParticleGun(randomSvcName=simFlags.RandomSvc.get_Value(), randomStream="SINGLE")
pg.sampler.pid = 22
pg.sampler.mom = PG.EEtaMPhiSampler(energy=20000, eta=[0.2,0.25]) 
job += pg

from TruthIO.TruthIOConf import DumpMC
dump=DumpMC()
job+=dump

include("G4AtlasApps/fragment.SimCopyWeights.py")
```

Location: */eos/atlas/atlascerngroupdisk/proj-simul/G4Run3/G4Tuning/photons/		ParticleGun.Photons\_E20\_eta020.py*

2. Simulation of single photon with E = 20 GeV, $\eta$ = 0.2
 * Setup the correct Athena version (above)
 * Move into the run directory
 * Use the following line

```python
$ Sim_tf.py --physicsList 'FTFP_BERT_ATL_VALIDATION' --truthStrategy 'MC15aPlus' --simulator 'FullG4' --conditionsTag 'default:OFLCOND-MC16-SDR-14' --DataRunNumber '284500' --preExec 'EVNTtoHITS:simFlags.TightMuonStepping=True' --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,/eos/atlas/atlascerngroupdisk/proj-simul/G4Run3/G4Tuning/photons/ParticleGun.Photons_E20_eta020.py' --geometryVersion 'default:ATLAS-R2-2016-01-00-01_VALIDATION' --postInclude 'default:PyJobTransforms/UseFrontier.py' --outputHITSFile "ParticleGun_pid22_E20000_eta_020.HITS.pool.root" --maxEvents 100 --randomSeed 123401
```

==Sanity Check==: cross-check the results with the outputs in */eos/atlas/atlascerngroupdisk/proj-simul/G4Run3/G4Tuning/photons/*

### Digitisation and Reconstruction: Output ESD and AOD files
* For Geant4 10.1, the HIT file production along with ESD/AOD files can be done in the same shell (in lxplus), ~~but for Geant4 10.6, this is not possible so that we should use a different shell with a diffferent Athena setup to conduct digitisations and reconstructions after the HIT file productions are finished. (This is due to the fact that Geant4 10.6 build in within an old Athena release)~~

* **Update(Feb.10)**: This issue is resolved with the updated version of Athena 21.0 (in one of the nightly builds).  One can now run all processes in the same shell (in lxplus) 

* Digitisation and Reconstruction with default MC16 Run-2 Settings, no pileup

```python
$ Reco_tf.py --digiSteeringConf 'StandardSignalOnlyTruth' --conditionsTag 'default:OFLCOND-MC16-SDR-25' --valid 'True' --postInclude 'default:PyJobTransforms/UseFrontier.py' --autoConfiguration 'everything' --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"]' --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' 'all:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doSlimming.set_Value_and_Lock(False)' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODSLIM");' --geometryVersion 'default:ATLAS-R2-2016-01-00-01' --inputHITSFile "ParticleGun_pid22_E20000_eta_020.HITS.pool.root" --outputAODFile "ParticleGun_pid22_E20000_eta_020.AOD.pool.root" --outputESDFile "ParticleGun_pid22_E20000_eta_020.ESD.pool.root" --maxEvents -1
```

==Sanity Check==: cross-check the results with the outputs in */eos/atlas/atlascerngroupdisk/proj-simul/G4Run3/G4Tuning/photons/*

### Egamma Ntuple Making and Photon Shower Shape Analysis
* One needs an executable to run xAOD -> ntuple with shower shapes.
* Instructions: 
 * Contact Joshua to receive the permission to access the gitlab directory *https://gitlab.cern.ch/jbeirer/XaodToNtuple/-/tree/master*
 * When one starts a new session or terminal, one should do the following (compile) in order to run the code **.xaodToNtuple** locally:

 ```
 $ mkdir source build run submit
 $ cd source
 $ git clone https://gitlab.cern.ch/jbeirer/XaodToNtuple.git
 $ ln -s ../build/CMakeLists.txt .
 ```
 
 * Do the following everytime one tries to run **./xaodToNtuple**:
 
  Copy the execution code into run directory (running locally when the files are not too big)
  
  ```
  $ cd ../build
  $ asetup 21.2,AnalysisBase,latest,here
  $ cmake ../source
  $ make -j
  $ cd ../run
  $ cp ../build/x86_64-centos7-gcc8-opt/bin/xaodToNtuple .
  $ ./xaodToNtuple input.root output.root
  ```
  
  For the Grid submission
  
  ```
  $ asetup 21.2,AnalysisBase,latest,here
  $ cd ../submit
  $ cp ../build/x86_64-centos7-gcc8-opt/bin/xaodToNtuple .	
  $ prun --exec="./xaodToNtuple %IN ntuple.root" --outputs=ntuple.root --tmpDir ../tmp_submit --noBuild --athenaTag AnalysisBase,21.2.142 --cmtConfig=x86_64-centos7-gcc8-opt --inDS= … --outDS= ...
  ```

  (**Reference**: [XaodToNtuple](https://gitlab.cern.ch/jbeirer/XaodToNtuple))

##Remark
* Multiple Run with Different Random Seeds
 
 When one tries to generate the samples for the full G4 simualtions, e.g. 5000 events, one should generate the samples with smaller event sizes with different random seeds (in order to avoid re-simulating identical events several times).  For example, one could generate 500 events with different random seeds, say 12341 ~ 123410.
 
 One can easily do this using the bash shell scripts(*/afs/cern.ch/user/d/dwkim/qt\_test\_area/job\_repeat.sh* and */afs/cern.ch/user/d/dwkim/code\_devel/aodToNtuple\_repeat.sh*) which will automatically insert lists of commands step-by-step.
 
```
#!/bin/bash

cd /afs/cern.ch/user/d/dwkim/qt_test_area
#setupATLAS
#asetup local/simulation/21.0,r2021-02-23T1818,Athena

#setup for 10mm rangecut EMZ option - EMB, EMEC
#mkdir single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_10mm_EMB_EMEC
#cd single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_10mm_EMB_EMEC
#source /afs/cern.ch/user/d/dwkim/qt_test_area/build_athena/x86_64-centos7-gcc62-opt/setup.sh

#setup for 0.3mm rangecut EMZ option - EMB, EMEC
mkdir single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_0.1mm_EMB_EMEC
cd single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_0.1mm_EMB_EMEC
source /afs/cern.ch/user/d/dwkim/qt_test_area/build_athena3/x86_64-centos7-gcc62-opt/setup.sh


# Variables

numberEvents=500
numberIteration=10

#generate HIT files in the current directory

for ((i=1; i<$numberIteration+1;i++)); do
    echo "*****************************"
    echo "Case $i = random seed 1234$i "
    echo "*****************************"
    python /cvmfs/atlas.cern.ch/repo/sw/software/21.0/Athena/21.0.120/InstallArea/x86_64-centos7-gcc62-opt/bin/Sim_tf.py --physicsList 'FTFP_BERT_EMZ_VALIDATION' --truthStrategy 'MC15aPlus' --simulator 'FullG4' --conditionsTag 'default:OFLCOND-MC16-SDR\
-14' --DataRunNumber '284500' --preExec 'EVNTtoHITS:simFlags.TightMuonStepping=True' --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,/eos/atlas/atlascerngroupdisk/proj-simul/G4Run3/G4Tuning/photons/ParticleGun.Photons_E20_eta02\
0.py' --geometryVersion 'default:ATLAS-R2-2016-01-00-01_VALIDATION' --postInclude 'default:PyJobTransforms/UseFrontier.py' --outputHITSFile "ParticleGun_pid22_E20000_eta_020_randomSeed_1234$i.HITS.pool.root" --maxEvents $numberEvents --randomSeed 1234$\
i
    #Digitization and Reconstruction: Output ESD and AOD files
    python /cvmfs/atlas.cern.ch/repo/sw/software/21.0/Athena/21.0.120/InstallArea/x86_64-centos7-gcc62-opt/share/Reco_tf.py --digiSteeringConf 'StandardSignalOnlyTruth' --conditionsTag 'default:OFLCOND-MC16-SDR-25' --valid 'True' --postInclude 'default\
:PyJobTransforms/UseFrontier.py' --autoConfiguration 'everything' --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"]' --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobpropert\
ies.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True\
);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' \
'all:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doSlimming.set_Value_and_Lock(False)' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODSLIM");' --geometryVersion 'de\
fault:ATLAS-R2-2016-01-00-01' --inputHITSFile "ParticleGun_pid22_E20000_eta_020_randomSeed_1234$i.HITS.pool.root" --outputAODFile "ParticleGun_pid22_E20000_eta_020_randomSeed_1234$i.AOD.pool.root" --outputESDFile "ParticleGun_pid22_E20000_eta_020_rando\
mSeed_1234$i.ESD.pool.root" --maxEvents -1

done
```

```
#!/bin/bash
numberIteration=10

for ((i=1; i<$numberIteration+1;i++)); do
    echo "*************************"
    echo "Conversion: AOD to Ntuple"
    echo "*************************"
    ./xaodToNtuple ParticleGun_pid22_E20000_eta_020_randomSeed_1234$i.AOD.pool.root ParticleGun_pid22_E20000_eta_020_randomSeed_1234$i.ntuple.pool.root
done
```

==**Tasks**==

- [ ] Combine the shell scripts to run everything in one shot
- [ ] Check to run the code in batch mode (store results in /aos/ but run in /afs/)


## Geant4 PhysicsList Checks (EM Physics)
* In order to run Geant4 PhysicsList with options \_EMV and \_EMX, I needed to comment out three lines in one of the python files in */afs/cern.ch/user/d/dwkim/qt\_test\_area/athena/Simulation/G4Atlas/G4AtlasTools/python/G4AtlasToolsConfig.py*.  
  
  To do this, I needed to rebuild Athena with the new edited code.  This can be achieved by using git and cmake/make.

### Git Basics ([click](https://gitlab.cern.ch/atlas/athena))
* Git is a platform used for the code development, sharing, and uploading the new versions of the softwares where one can edit one's own code without affecting the publically available softwares.  In the case of Athena, one can setup Athena, make changes, and test the changed version of Athena in his or her own location after setting it up with the git system to fit the need of the user's purpose. Check the following sites: [Environment Setup](https://atlassoftwaredocs.web.cern.ch/gittutorial/env-setup/) and [Modifying Athena Package](https://kkrizka.github.io/atlas-cmake/07-modifyingathena/index.html)

* Environment Setup

  ```
$ ssh -Y -l dwkim lxplus.cern.ch
$ setupATLAS
$ lsetup git
```

  ```
$ git config --list # check your name and email address are properly set
$ git config --global user.name "Your Name"
$ git config --global user.email "your.name@cern.ch"
```

  Additional Recommendations:

  ```
$ git config --global push.default simple
$ git config --global http.postBuffer 1038576000 # this makes some operations more straightforward
$ git config --global http.emptyAuth true # Required on CC7, addresses an issue iwth libcurl and krb5
```

* Fork the Repository

In the [website](https://gitlab.cern.ch/atlas/athena), create your own copy of the repository by pressing the Fork buttom on the project's front page.  And add **ATLAS Robot** with **Developer** option.

* Clone Repository Locally (I chose **Sparse Checkout**)

  (1) Full Checkout: gives you access to working copies of all files.
  
  (2) Sparse Checkout: gives you only the parts of the repository you want to update.

  Inside one's working directory,

  ```
$ setupATLAS
$ lsetup git
$ git atlas init-workdir http://:@gitlab.cern.ch:8443/atlas/athena.git # creates the athena directory in our case
$ cd athena
$ git atlas addpkg G4AtlasTools # other options: rmpkg, listpkg
```

* Develop Code (Make changes for running some selection cuts, etc)

  (1) Create a Topic Branch
  
  ```
  $ git fetch upstream # makes sure that your local repository is completely up-to-date
  ```

  ```
  $ git checkout -b 21.0-G4-EMV_EMX_enabled upstream/21.0 --no-track
  ```

* Useful Commands

  ```
$ git branch -vv
$ git status # This shows the changes one made
$ git add Simulation/G4Atlas/G4AtlasTools/python/G4AtlasToolsConfig.py
$ git commit -m "Geant4 Physics Lists _EMV _EMX Enabled"
```

  - **commit**: this is equivalent to the save point where one should do as often as possible when one makes the changes.  One can also go back to the previous save point by checking the flag number using the following commands.

  ```
$ git log
$ git checkout [to-that-specific-commit-flag-number]
```

 (2) Compile
  
  After one edits files(codes) inside athena/... , one needs to build and compile the edited version of Athena.
  
  ```
  $ mkdir ../build
  $ cd ../build
  $ asetup local/simulation/21.0,r2021-02-23T1818,Athena
  ```

  Then, do the following to include the path of the edited package that needs to be additionally compiled along with the other "default" Athena packages before "make".  (Still in "build" directory)
  
  ```
  $ cp ../athena/Projects/WorkDir/package_filters_example.txt ../package_filters.txt
  $ emacs -nw ../package_filters.txt
  ```
  
  Inside the *../package_filters.txt*, add the following lines:
  
  ```
  + athena/Simulation/G4Atlas/G4AtlasTools
  - athena/.*
  ```
  
  Then,
  
  ```
  $ cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
  $ make -j
  ```
  
  **Remark**: ~~When running the first time with the predefined setup for the G4AtlasTools, there was an error message~~:
  
  ```
   [ 33%] Building CXX object Simulation/G4Atlas/G4AtlasTools/CMakeFiles/G4AtlasToolsLib.dir/src/AddPhysicsDecayTool.cxx.o
/afs/cern.ch/user/d/dwkim/qt_test_area/athena/Simulation/G4Atlas/G4AtlasTools/src/AddPhysicsDecayTool.cxx: In member function 'virtual void AddPhysicsDecayTool::ConstructProcess()':
/afs/cern.ch/user/d/dwkim/qt_test_area/athena/Simulation/G4Atlas/G4AtlasTools/src/AddPhysicsDecayTool.cxx:27:26: error: 'aParticleIterator' was not declared in this scope
 #define PARTICLEITERATOR aParticleIterator
                          ^
/afs/cern.ch/user/d/dwkim/qt_test_area/athena/Simulation/G4Atlas/G4AtlasTools/src/AddPhysicsDecayTool.cxx:27:26: note: in definition of macro 'PARTICLEITERATOR'
 #define PARTICLEITERATOR aParticleIterator
                          ^~~~~~~~~~~~~~~~~
make[2]: *** [Simulation/G4Atlas/G4AtlasTools/CMakeFiles/G4AtlasToolsLib.dir/src/AddPhysicsDecayTool.cxx.o] Error 1
make[1]: *** [Simulation/G4Atlas/G4AtlasTools/CMakeFiles/G4AtlasToolsLib.dir/all] Error 2
This has been fixed by editting "athena/Simulation/G4Atlas/G4AtlasTools/src/AddPhysicsDecayTool.cxx",
  ```
  
  ==**Fixed**== (suggested by John Apostolakis - Mar. 8, 2021)
  
  ```
  aParticleIterator, theParticleIterator -> GetParticleIterator() # change made in L26
  ```
  
  The new Athena setup is now complete and one can run \_EMX, \_EMV options with the following setup.  First go to the directory that you run your production (HIT file generation) and setup the following.
  
  ```
  $ cd ../run
  $ source ../build/x86_64-slc6-gcc49-opt/setup.sh
  ```
  
  Then, run the simulation codes.
  
  
**Follow-up Tasks**

- [x] Learn **git** in Youtube (basics)

- [x] Learn **uproot** (which enables the use of python syntax and packages) 

### Analysis (Optimization)

* After one produces the ntuple using the previous setups and running the samples in the lxplus, the following observables are investigated in order to look at the photon shower shapes using histograms:

![DetectorSetup](Screenshot 2021-03-31 at 15.40.49.png)

![ParticleIdentification](Screenshot 2021-03-31 at 16.06.45.png)

![LateralShowerShape](Screenshot 2021-03-31 at 16.08.31.png)

**Reference**: Simulation of Electrons and Photons in ATLAS (2,3 November 2020), a talk by Maarten Boonekamp

 (1) **Reta** - in ntuple (tree: photons)

 (2) **Rphi** - in ntuple (tree: photons)

 (3) **weta2** - in ntuple (tree: photons)

 (4) **Energy Response** (**$\text{E}/\text{E}_{\text{True}}$**):
  
 - With the transverse momentum $\text{p}_{\text{T}}$, polar angles in the transverse plane $\phi$, and pseudorapidity $\eta$ (which are the variables in the ntuple), one can use the following relations to find the energy of the particle.

 $\text{p}_{\text{x}} = \text{p}_{\text{T}} \cos \phi$

 $\text{p}_{\text{y}} = \text{p}_{\text{T}} \sin \phi$
 
 $\text{p}_{\text{z}} = \text{p}_{\text{T}} \sinh \eta$

 $\Rightarrow$ $|\text{p}| = \text{p}_{\text{T}} \cosh \eta $
 
 - For the mass-less case (e.g. photon) or particles travelling close to c, $|\text{p}| \sim \text{E}$.  In addition, $\text{E}_{\text{True}}$ can be found from HIT files (variable: **mc_e**; if there are more than one entry in this variable, take the first entry).  In our case, it is 20 GeV since we are running the simulation with ParticleGuns for gamma 20GeV eta0.2.  

(5) **Energy Resolution** 

 - After running over different rangecuts for Electromagnetic Barrel(EMB) and Electromagnetic End Cap(EMEC), say 0.1mm(default), 0.3mm, 1.0mm, 10mm, (a single production threshold as a baseline - 0.1mm) we can plot the energy resolution.
 
 $\text{Energy Resolution} = \frac{\text{RMS} \left( \text{E} / \text{E}_{\text{True}} \right)}{\text{Mean} \left( \text{E} / \text{E}_{\text{True}} \right)}$ 
 
 - For the benchmark, here is the result of Vladimir Ivantchenko's plots for different range cuts.
 
 ![rangecuts](Atlasbar-EMval-G4-10.6p3-vs-10.4p2.png)
 
### The Analysis Results

#### (1) Photon Shower Distribution (unconverted + $w_{\eta 1}$ $>$ -1)

- The analysis is done with "uproot" - .ipynb available for double-check.

- _Remark_: for PhysicsList options $\textit{ATL}$ only changes hadronic physics, so we do not expect any change (and actually have not seen any change) when using or not using ATL for electrons or photons.

- When drawing the above observables (5) and the fudge-factor(FF) values - comparison between Geant 4 10.6 with different range cuts and Geant4 10.1 (default RangeCut 0.1mm for EMB/EMEC), unconverted photons are chosen for plotting the shower shape.  

  **_Unconverted photons vs. Converted photons_**: [EGammaIdentificationRun2](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EGammaIdentificationRun2) 
  
 - These are two different categories of reconstructed photons.  Converted photons interact in the inner detector and turn into an electron-positron pair and we reconstruct an object that is an EM cluster matched to two tracks originating from a fitted conversion vertex (at least in the best case).  On the other hand, unconverted photons consists in an EM cluster without any track pointing to it.  Hence, the shower shapes for the two different types of photons are different as in one case the shower is caused by a photon that reach the calorimeter undisturbed in comparison to a electron-positron pair in the other case. 
  
   One can access this information using the variable called "conversionType" (reco-level information):
  
   ```python
  # unconverted = 0
  # converted = all the rest (1,2,3,4,5,..)
 ```
 - In the process, the following tables([Index of /atlas-groupdata/EGammaVariableCorrection/TUNE22](https://atlas-groupdata.web.cern.ch/atlas-groupdata/EGammaVariableCorrection/TUNE22/) - from Julien Maurer) have been used for calculations of the FF values.  In addition, the graphical representation of the FF values for corresponding energies are shown below.
  
  ![ff_values_plots](Screenshot 2021-05-06 at 13.13.03.png)
  **_Reference_**: $\textit{Photon Lateral Shower Shapes}$ in Simulation Meeting Jul.9, 2020 by J. Maurer

- Similar study has been done by J. Maurer in the photon shower shape.  The distribution from his slides would be a good sanity check for the work I am doing.

![Julien_Maurer_result](Screenshot 2021-05-06 at 13.18.13.png)
**_Reference_**: $\textit{Photon Lateral Shower Shapes}$ in Simulation Meeting Jul.9, 2020 by J. Maurer

- After the unconverted photons are plotted for each observable, Joshua pointed out that the $w_{\eta 1}$ distributions have negative means and FF values differ by a factor of 10.  This is due to the fact that $w_{\eta 1}$ can indeed be -999 if the summed energy of the cells left and right of the hottest cell (excluding that one), is <=0, which could well happen at very low photon energy.

- For the shower distribution plots, hence, the following selections are chosen for investigation and comparison to the Geant4 10.1 (Data: MC16 / GeometryVersion: ATLAS-R2-2016-01-00-01_VALIDATION).

### Energy Response

![energy_response](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Energy_response.pdf)

![energy_repsonse_version_compare](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Energy_resolution.pdf)

### $R_{\eta}$

![R_eta_histo](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Reta_histogram.pdf)

![R_eta_ratio](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Reta_Ratio_plot.pdf)

![R_eta_ffvalue](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Reta_FFvalue_plot.pdf)

### $R_{\phi}$

![R_phi_histo](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Rphi_histogram.pdf)

![R_phi_ratio](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Rphi_Ratio_plot.pdf)

![R_phi_ffvalue](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_Rphi_FFvalue_plot.pdf)

### $w_{\eta 2}$

![R_weta2_histo](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_weta2_histogram.pdf)

![R_weta2_ratio](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_weta2_Ratio_plot.pdf)

![R_weta2_ffvalue](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_weta2_FFvalue_plot.pdf)

### $w_{\eta 1}$

![R_weta1_histo](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_weta1_histogram.pdf)

![R_weta1_ratio](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_weta1_Ratio_plot.pdf)

![R_weta1_ffvalue](single_unconverted_photon_weta1_select_E20000_eta0.2_G4_101_106_EMZ_RangeCuts_EMB_EMEC_weta1_FFvalue_plot.pdf)



#### (2) Simulation Runtime (HITS.pool.root production for 5000 events) 

- First, when one tries to combine the ---.HITS.pool.root, one can use the built-in python code for combining HITS files as ```hadd``` may not work properly for HITS files: 

```python
HITSMerge_tf.py --maxEvents='2000' --inputHITSFile='input_1.HITS.pool.root,input_2.HITS.pool.root,input_3.HITS.pool.root' --outputHITS_MRGFile='output.merge.HITS.pool.root' --skipEvents=0 --postInclude='RecJobTransforms/UseFrontier.py'
```

**Reference**: [NSWL1TriggerInAthena](https://twiki.cern.ch/twiki/bin/view/Sandbox/NSWL1TriggerInAthena) 

- When trying to access the HITS files, one should work in the _lxplus_ setting with the Athena setup.  As I tried to open the file in the local ROOT setup (in my own laptop), it produces many warning messages when trying to access the HITS files due to the lack of connection to the Athena (initial setting done by ```setupATLAS``` and ```asetup 21.x.xxx```).  Hence, it is beneficial to work in lxplus where the access is possible without further setups.

- In addition, when accessing the HITS files, the uproot does not seem to work either.  I used "pyroot" for writing a code (only for reading the timing information coming from HITS files).

```python
import ROOT as r
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict

######################################### #
# Load the HITS.root files                #
# Call an appropriate leaf from the tree. #
###########################################

dir_g4_106_0_1 = str("single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_0.1mm_EMB_EMEC/output.merge.HITS.pool.root")
dir_g4_106_0_3 = str("single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_0.3mm_EMB_EMEC/output.merge.HITS.pool.root")
dir_g4_106_1_0 = str("single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_1mm_EMB_EMEC/output.merge.HITS.pool.root")
dir_g4_106_10  = str("single_photon_20GeV_eta0.2_g4_10_6_EMZ_rangecut_10mm_EMB_EMEC/output.merge.HITS.pool.root")
dir_g4_101     = str("single_photon_20GeV_eta0.2_5000events/output.merge.HITS.pool.root")

File_g4_106_0_1 = r.TChain("CollectionTree")
File_g4_106_0_3 = r.TChain("CollectionTree")
File_g4_106_1_0 = r.TChain("CollectionTree")
File_g4_106_10  = r.TChain("CollectionTree")
File_g4_101     = r.TChain("CollectionTree")

File_g4_106_0_1.Add(dir_g4_106_0_1)
File_g4_106_0_3.Add(dir_g4_106_0_3)
File_g4_106_1_0.Add(dir_g4_106_1_0)
File_g4_106_10.Add(dir_g4_106_10)
File_g4_101.Add(dir_g4_101)

Numb_entry_g4_106_0_1 = File_g4_106_0_1.GetEntries()
Numb_entry_g4_106_0_3 = File_g4_106_0_3.GetEntries()
Numb_entry_g4_106_1_0 = File_g4_106_1_0.GetEntries()
Numb_entry_g4_106_10  = File_g4_106_10.GetEntries()
Numb_entry_g4_101     = File_g4_101.GetEntries()
"""
print('Number of Entries for G4 10.6 0.1mm = %f'%Numb_entry_g4_106_0_1)
print('Number of Entries for G4 10.6 0.3mm = %f'%Numb_entry_g4_106_0_3)
print('Number of Entries for G4 10.6 1.0mm = %f'%Numb_entry_g4_106_1_0)
print('Number of Entries for G4 10.6 10 mm = %f'%Numb_entry_g4_106_10)
print('Number of Entries for G4 10.1 0.1mm = %f'%Numb_entry_g4_101)
"""

##########################
# Looping for the events #
##########################
timings = defaultdict(float)

timing_cumulative = 0.0
for i in range(Numb_entry_g4_106_0_1):
    Entry = File_g4_106_0_1.GetEntry(i)
    timing_cumulative += File_g4_106_0_1.RecoTimingObj_p1_EVNTtoHITS_timings.timings[0]
timings['g4_106_0_1']=timing_cumulative
print('G4 10.6 0.1mm = %f'%timings['g4_106_0_1'])
#print(timing_cumulative)
#print(type(timing_cumulative))

timing_cumulative = 0.0
for i in range(Numb_entry_g4_106_0_3):
    Entry = File_g4_106_0_3.GetEntry(i)
    timing_cumulative += File_g4_106_0_3.RecoTimingObj_p1_EVNTtoHITS_timings.timings[0]
timings['g4_106_0_3']=timing_cumulative
print('G4 10.6 0.3mm = %f'%timings['g4_106_0_3'])

timing_cumulative = 0.0
for i in range(Numb_entry_g4_106_1_0):
    Entry = File_g4_106_1_0.GetEntry(i)
    timing_cumulative += File_g4_106_1_0.RecoTimingObj_p1_EVNTtoHITS_timings.timings[0]
timings['g4_106_1_0']=timing_cumulative
print('G4 10.6 1.0mm = %f'%timings['g4_106_1_0'])

timing_cumulative = 0.0
for i in range(Numb_entry_g4_106_10):
    Entry = File_g4_106_10.GetEntry(i)
    timing_cumulative += File_g4_106_10.RecoTimingObj_p1_EVNTtoHITS_timings.timings[0]
timings['g4_106_10']=timing_cumulative
print('G4 10.6 10 mm = %f'%timings['g4_106_10'])

timing_cumulative = 0.0
for i in range(Numb_entry_g4_101):
    Entry = File_g4_101.GetEntry(i)
    timing_cumulative += File_g4_101.RecoTimingObj_p1_EVNTtoHITS_timings.timings[0]
timings['g4_101']=timing_cumulative
print('G4 10.1 0.1mm = %f'%timings['g4_101'])
"""
timing_label=['g4_106_0_1','g4_106_0_3','g4_106_1_0','g4_106_10','g4_101']
print(type(timing_label))
timing_label = np.array(timing_label)
print(type(timing_label))
"""
timing_label = [1,2,3,4,5]
#print(type(timing_label))

timing_array=[]
timing_array.append(timings['g4_106_0_1'])
timing_array.append(timings['g4_106_0_3'])
timing_array.append(timings['g4_106_1_0'])
timing_array.append(timings['g4_106_10'])
timing_array.append(timings['g4_101'])


print(timing_array)

plt.figure(figsize=(14,12))
plt.plot(timing_label,timing_array,'o',color='b',markersize=12)
plt.title('CPU runtime for G4 10.6 + 10.1 (HITS.root)',fontsize=20)
plt.xlabel('G4 Version + Range Cut (EMEC/EMB)',ha='right',x=1.0,fontsize=15)
plt.ylabel('Time (unit?)',ha='right', y=1.0, fontsize=15)
plt.xticks([1,2,3,4,5],('g4_106_0_1','g4_106_0_3','g4_106_1_0','g4_106_10','g4_101'))
plt.xlim([0.5,5.5])
plt.grid(linestyle='--')
plt.savefig("cpu_runtime_G4_106_101_em_options.pdf")
```

- The printout of each process' timing is provided as follows: (in units of seconds) 

_Reference_: [unit](https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Reconstruction/RecEventTPCnv/src/RecoTimingObjCnv_p1.cxx#0075) [unit2](https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Reconstruction/RecAlgs/src/TimingAlg.cxx#0142)

```
G4 10.6 0.1mm = 9496780.866241
G4 10.6 0.3mm = 8311753.953140
G4 10.6 1.0mm = 9328702.722809
G4 10.6 10 mm = 6622999.030304
G4 10.1 0.1mm = 3065686.693268
```

- The above numbers do not seem reasonable.  _Possible reason_: the ntuple has one entry per photon, not per event, so I'm not sure it's trivial to count photons per event.