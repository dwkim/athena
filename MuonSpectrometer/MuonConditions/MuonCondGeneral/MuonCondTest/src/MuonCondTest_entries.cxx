/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
#include "MuonCondTest/AlignCondAthTest.h"
#include "MuonCondTest/MuonConditionsTestAlg.h" 
#include "MuonCondTest/MDTConditionsTestAlg.h"
#include "MuonCondTest/MDTConditionsTestAlgMT.h"
#include "MuonCondTest/CSCConditionsTestAlgMT.h"
#include "MuonCondTest/RPCStatusTestAlg.h"
#include "MuonCondTest/MuonConditionsHistoSummary.h"

DECLARE_COMPONENT( AlignCondAthTest )
DECLARE_COMPONENT( MuonConditionsTestAlg )
DECLARE_COMPONENT( MDTConditionsTestAlg )
DECLARE_COMPONENT( MDTConditionsTestAlgMT )
DECLARE_COMPONENT( CSCConditionsTestAlgMT )
DECLARE_COMPONENT( RPCStatusTestAlg )
DECLARE_COMPONENT( MuonConditionsHistoSummary )
