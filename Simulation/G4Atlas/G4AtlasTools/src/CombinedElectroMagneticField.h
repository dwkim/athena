// ----------------------------------------------------------------------------------- //
// Implementation of the Electric Field for G4 PHysicslist simulation - Sept. 30, 2021 //
// ----------------------------------------------------------------------------------- //


#ifndef G4ATLASTOOLS_COMBINEDELECTROMAGNETICFIELD_H
#define G4ATLASTOOLS_COMBINEDELECTROMAGNETICFIELD_H

// Geant4 includes
#include "G4ThreeVector.hh"
#include "G4MagneticField.hh"
#include "G4ElectroMagneticField.hh"
#include "G4UniformElectricField.hh"
//#include "G4ElectricField.hh"

class CombinedElectroMagneticField : public G4ElectroMagneticField
{
  
 public:
  //Constructors
  CombinedElectroMagneticField();
  //~CombinedElectroMagneticField();

  //Destructors
  virtual ~CombinedElectroMagneticField(); //  Else  delete fBfieldPtr;} to take ownership!
 
  CombinedElectroMagneticField(const CombinedElectroMagneticField& r) = delete;
  CombinedElectroMagneticField& operator = (const CombinedElectroMagneticField& p) = delete;
  // Copy constructor & assignment operator.

  CombinedElectroMagneticField(G4MagneticField* Bfield, G4ThreeVector Efield);
  CombinedElectroMagneticField(G4MagneticField* Bfield, float EfieldVec[3]);
  CombinedElectroMagneticField(G4MagneticField* Bfield, G4UniformElectricField* Efield);
  //CombinedElectroMagneticField(G4MagneticField* Bfield, G4ThreeVector Efield);
 
  void SetElectricField(G4ThreeVector Efield);
  void SetElectricField(float EfieldVec[3]);
  void SetElectricField(G4UniformElectricField* Efield);
  void SetMagneticField(G4MagneticField* Bfield);
  
  G4bool DoesFieldChangeEnergy() const { return true; };
  // Since an electric field can change track energy
  
  void GetFieldValue(const G4double Point[4], G4double* BEfieldValue) const override final;

 private:
  G4double         m_fElectricFieldXYZ[3];
  G4MagneticField* m_fBfieldPtr;  // Does not obtain ownership of object (currently)
  G4UniformElectricField* m_fEfieldPtr;
};

//CombinedElectroMagneticField::CombinedElectroMagneticField() : G4ElectroMagneticField() {}
//CombinedElectroMagneticField::~CombinedElectroMagneticField() {}

#endif
