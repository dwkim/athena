// From this package
#include "CombinedElectroMagneticField.h"

// Geant4 includes
#include "G4ThreeVector.hh"
#include "G4MagneticField.hh"
#include "G4ElectroMagneticField.hh"
#include "G4ElectricField.hh"
#include "G4UniformElectricField.hh"

// standard c++
#include <cassert>

CombinedElectroMagneticField::CombinedElectroMagneticField() : G4ElectroMagneticField() {}

CombinedElectroMagneticField::~CombinedElectroMagneticField() {}
 
void CombinedElectroMagneticField::SetElectricField(G4ThreeVector Efield)
{
  m_fElectricFieldXYZ[0] = Efield.x();
  m_fElectricFieldXYZ[1] = Efield.y();
  m_fElectricFieldXYZ[2] = Efield.z();
}
 
void CombinedElectroMagneticField::SetElectricField(float EfieldVec[3])
{
  for(int i=0; i<3; i++)
    m_fElectricFieldXYZ[i] = EfieldVec[i];
}
 
void CombinedElectroMagneticField::SetMagneticField(G4MagneticField* Bfield)
{
  m_fBfieldPtr= Bfield;
  assert( Bfield != nullptr);
}

CombinedElectroMagneticField::CombinedElectroMagneticField(G4MagneticField* Bfield, G4UniformElectricField* Efield)
{
  this->SetMagneticField(Bfield);
  this->SetElectricField(Efield);
}

void CombinedElectroMagneticField::SetElectricField(G4UniformElectricField* Efield)
{
  m_fEfieldPtr=Efield;
  assert(Efield != nullptr);
}
 
inline CombinedElectroMagneticField::CombinedElectroMagneticField(G4MagneticField* Bfield, G4ThreeVector Efield)
{
  this->SetMagneticField(Bfield);
  this->SetElectricField(Efield);
}
 
inline CombinedElectroMagneticField::CombinedElectroMagneticField(G4MagneticField* Bfield, float EfieldVec[3])
{
  this->SetMagneticField(Bfield);
  this->SetElectricField(EfieldVec);
}
 
void CombinedElectroMagneticField::GetFieldValue(const G4double Point[4], G4double* EBfield) const
{
  m_fBfieldPtr->GetFieldValue( Point, EBfield ); // Fills EBfield[0] to [2]
  //EBfield[3]= m_fElectricFieldXYZ[0];
  //EBfield[4]= m_fElectricFieldXYZ[1];
  //EBfield[5]= m_fElectricFieldXYZ[2];
  m_fEfieldPtr->GetFieldValue( Point, EBfield ); // Fills EBfield[3] to [5]
}
