/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

// Main header
#include "G4AtlasTools/DetectorFieldManagerTool.h"

// From this package
#include "TightMuonElseNoFieldManager.h"
#include "SwitchingFieldManager.h"
#include "CombinedElectroMagneticField.h"

// Geant4 includes
#include "G4ChordFinder.hh"
#include "G4FieldManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4MagneticField.hh"
#include "G4Version.hh"
#include "G4VIntegrationDriver.hh"
#include "G4EqMagElectricField.hh"
#include "G4UniformElectricField.hh"
#include "G4DormandPrince745.hh"
#include "G4TransportationManager.hh"
#include "G4IntegrationDriver.hh"
#include "G4SystemOfUnits.hh"
#include "G4MagneticField.hh"
#include "G4ThreeVector.hh"

//-----------------------------------------------------------------------------
// Tool constructor
//-----------------------------------------------------------------------------
DetectorFieldManagerTool::DetectorFieldManagerTool(const std::string& type,
                                                   const std::string& name,
                                                   const IInterface* parent)
  : G4FieldManagerToolBase(type, name, parent)
{
}

//-----------------------------------------------------------------------------
// Initialize a field manager
//-----------------------------------------------------------------------------
StatusCode DetectorFieldManagerTool::initializeField()
{
  ATH_MSG_DEBUG("initializeField");

  if (m_fieldOn) {

    // If field manager already exists for current thread, error.
    // There is no foreseen use-case for this situation.
    if(m_fieldMgrHolder.get()) {
      ATH_MSG_ERROR("DetectorFieldManagerTool::initializeField() - " <<
                    "Field manager already exists!");
      return StatusCode::FAILURE;
    }

    // Retrieve the G4MagneticField
    G4MagneticField* field = m_fieldSvc->getField();

    // ------------------------------------------------------------------------ //
    // Set the Electric Field in Calo (Nov.8, 2021 - dong.won.kim@cern.ch)
    
    // E field Default - 0 kV/cm
    //auto pEfield = new G4UniformElectricField(G4ThreeVector(0.0,0.0,0.0));
    auto pEfield = new G4UniformElectricField(10.0*kilovolt/cm,0.0,0.0);
    // E field implementation in radial direction -> G4UniformElectricField (G4double vField, G4double vTheta, G4double vPhi)
    auto pEMfield = new CombinedElectroMagneticField(field,pEfield);
    G4EqMagElectricField* pEquation = new G4EqMagElectricField(pEMfield);
    
    G4int nvar = 8;

    // Create the Runge-Kutta 'stepper' using the efficient 'DoPri5' method
    auto pStepper = new G4DormandPrince745(pEquation, nvar);
    G4double minStep = 0.010*mm ; // minimal step of 10 microns    
    // ------------------------------------------------------------------------ //

    // Create a new field manager
    G4FieldManager * fieldMgr = nullptr;
    if (m_muonOnlyField){
      // fieldMgr = new TightMuonElseNoFieldManager();
      fieldMgr = new SwitchingFieldManager(field);
      //fieldMgr = new SwitchingFieldManager(pEMfield); // Nov.8,2021
    } else {
      fieldMgr = new G4FieldManager();
      //fieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager(); // Nov.8,2021
    }
    
    // Save it in the TL holder
    m_fieldMgrHolder.set(fieldMgr);

    fieldMgr->SetDetectorField(pEMfield); // Nov.8,2021
    ATH_CHECK( setFieldParameters(fieldMgr) );
    
    // Create and configure the ChordFinder
    // fieldMgr->CreateChordFinder(field);

#if G4VERSION_NUMBER < 1040
    ATH_MSG_DEBUG("Old style stepper setting");
    G4MagIntegratorStepper* stepper = getStepper(m_integratorStepper, field); // did not change field -> pEMfield since using Geant4 10.6
    G4MagInt_Driver* magDriver = fieldMgr->GetChordFinder()->GetIntegrationDriver();
    magDriver->RenewStepperAndAdjust(stepper);
#else
    ATH_MSG_DEBUG("New style stepper setting");
    //G4VIntegrationDriver* driver = createDriverAndStepper(m_integratorStepper, field);
    //G4ChordFinder* chordFinder = fieldMgr->GetChordFinder();
    auto pIntgrationDriver =
      new G4IntegrationDriver<G4DormandPrince745>(minStep,
						  pStepper,
						  nvar); // Nov.8,2021
    G4ChordFinder* pChordFinder = new G4ChordFinder(pIntgrationDriver); // Nov.8,2021    
    fieldMgr->SetChordFinder(pChordFinder); // Nov.8,2021
    pChordFinder->SetIntegrationDriver(pIntgrationDriver);
#endif

    // Assign the field manager to volumes
    if (!m_logVolumeList.empty()) {
      auto logVolStore = G4LogicalVolumeStore::GetInstance();
      for (const auto& volume: m_logVolumeList) {
        G4LogicalVolume* logicalVolume = logVolStore->GetVolume(volume);
        if (logicalVolume != nullptr) logicalVolume->SetFieldManager(fieldMgr, true);
        else
          ATH_MSG_WARNING("No volume called " << volume << " was found in the G4LogicalVolumeStore! Skipping this volume.");
      }
    }
    else if (!m_physVolumeList.empty()) {
      auto physVolStore = G4PhysicalVolumeStore::GetInstance();
      for (const auto& volume: m_physVolumeList) {
        G4VPhysicalVolume* physicalVolume = physVolStore->GetVolume(volume);
        if (physicalVolume != nullptr) physicalVolume->GetLogicalVolume()->SetFieldManager(fieldMgr, true);
        else
          ATH_MSG_WARNING("No volume called " << volume << " was found in the G4PhysicalVolumeStore! Skipping this volume.");
      }
    }
    else
      ATH_MSG_WARNING("No volumes are provided. Field manager is NOT assigned.");

  }

  return StatusCode::SUCCESS;
}
