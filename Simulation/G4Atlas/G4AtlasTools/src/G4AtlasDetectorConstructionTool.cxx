/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// Include files

// local
#include "G4AtlasTools/G4AtlasDetectorConstructionTool.h"
#include "G4AtlasTools/G4FieldManagerToolBase.h"

// Implementation of EB Field Oct.1,2021
#include "CombinedElectroMagneticField.h"
#include "G4EqMagElectricField.hh"
#include "G4UniformElectricField.hh"
#include "G4DormandPrince745.hh"
#include "G4TransportationManager.hh"
#include "G4IntegrationDriver.hh"
#include "G4ChordFinder.hh"
#include "G4SystemOfUnits.hh"
#include "G4MagneticField.hh"
#include "G4ThreeVector.hh"

//G4ElectricField*        pEfield;
//G4EqMagElectricField*   pEquation;
//G4ChordFinder*          pChordFinder;

//-----------------------------------------------------------------------------
// Implementation file for class : G4AtlasDetectorConstructionTool
//
// 2014-10-03: Andrea Dell'Acqua
//-----------------------------------------------------------------------------


//=================================
// Standard constructor, initializes variables
//=================================
G4AtlasDetectorConstructionTool::G4AtlasDetectorConstructionTool( const std::string& type,
                                                                  const std::string& nam,const IInterface* parent )
  : base_class( type, nam , parent )
{
}

//=================================
// Athena method overrides
//=================================
StatusCode G4AtlasDetectorConstructionTool::initialize( )
{
  ATH_MSG_DEBUG( "Initializing Geometry configuration tools "  );
  for (auto it: m_configurationTools)
  {
    ATH_CHECK( it.retrieve() );
    ATH_CHECK( it->preGeometryConfigure() );
  }

  ATH_MSG_DEBUG( "Initializing World detectors in " << name() );
  ATH_CHECK( m_detTool.retrieve() );

  ATH_MSG_DEBUG( "Initializing sensitive detectors in " << name() );
  ATH_CHECK( m_senDetTool.retrieve() );

  ATH_MSG_DEBUG( "Setting up G4 physics regions" );
  for (auto& it: m_regionCreators)
  {
    ATH_CHECK( it.retrieve() );
  }

  if (m_activateParallelWorlds)
  {
    ATH_MSG_DEBUG( "Setting up G4 parallel worlds" );
    for (auto& it: m_parallelWorlds)
    {
      ATH_CHECK( it.retrieve() );
    }
  }

  ATH_MSG_DEBUG( "Setting up field managers" );
  ATH_CHECK( m_fieldManagers.retrieve() );

  return StatusCode::SUCCESS;
}

std::vector<std::string>& G4AtlasDetectorConstructionTool::GetParallelWorldNames()
{
  return m_parallelWorldNames;
}

//=================================
// G4VUserDetectorConstruction method overrides
//=================================
G4VPhysicalVolume* G4AtlasDetectorConstructionTool::Construct()
{
  ATH_MSG_DEBUG( "Detectors " << m_detTool.name() <<" being set as World" );
  m_detTool->SetAsWorld();
  m_detTool->Build();

  ATH_MSG_DEBUG( "Setting up G4 physics regions" );
  for (auto& it: m_regionCreators)
  {
    it->Construct();
  }

  if (m_activateParallelWorlds)
  {
    ATH_MSG_DEBUG( "Setting up G4 parallel worlds" );
    for (auto& it: m_parallelWorlds)
    {
      m_parallelWorldNames.push_back(it.name());
      this->RegisterParallelWorld(it->GetParallelWorld());
    }
  }

  ATH_MSG_DEBUG( "Running geometry post-configuration tools" );
  for (auto it: m_configurationTools)
  {
    StatusCode sc = it->postGeometryConfigure();
    if (!sc.isSuccess())
    {
      ATH_MSG_FATAL( "Unable to run post-geometry configuration for " << it->name() );
    }
  }

  return m_detTool->GetWorldVolume();
}

void G4AtlasDetectorConstructionTool::ConstructSDandField()
{
  ATH_MSG_DEBUG( "Setting up sensitive detectors" );
  if (m_senDetTool->initializeSDs().isFailure())
  {
    ATH_MSG_FATAL("Failed to initialize SDs for worker thread");
  }

  ATH_MSG_DEBUG( "Setting up field managers" );
  for (auto& fm : m_fieldManagers)
  {
    StatusCode sc = fm->initializeField();
    if (!sc.isSuccess())
    {
      ATH_MSG_FATAL( "Unable to initialise field with " << fm->name() );
      return;
    }
  }
  /*
  // Implementation of EM Field  -  Oct. 1, 2021
  ATH_MSG_DEBUG("Integrating EM Fields");
  
  G4MagneticField* pAtlasMagneticField = m_fieldSvc->getField();
  //pEfield = new G4UniformElectricField(G4ThreeVector(0.0,100000.0*kilovolt/cm,0.0));
  //G4ElectricField* pEfield = new G4UniformElectricField(G4ThreeVector(0.0,0.0,0.0));
  //G4ThreeVector pEfield;
  //G4ThreeVector pEfield = new G4UniformElectricField(G4ThreeVector(0.0,0.0,0.0))->GetFieldValue();
  G4ThreeVector pEfield = G4ThreeVector(0,0,0);
  auto pEMfield = CombinedElectroMagneticField(pAtlasMagneticField,pEfield); 
  G4EqMagElectricField* pEquation = new G4EqMagElectricField(pEMfield);

  G4int nvar = 8;

  // Create the Runge-Kutta 'stepper' using the efficient 'DoPri5' method
  auto pStepper = new G4DormandPrince745(pEquation, nvar);

  // Get the global field manager
  auto fieldManager= G4TransportationManager::GetTransportationManager()->
    GetFieldManager();
  // Set this field to the global field manager
  fieldManager->SetDetectorField(pEMfield);

  G4double minStep = 0.010*mm ; // minimal step of 10 microns

  // The driver will ensure that integration is control to give
  //   acceptable integration error
  auto pIntgrationDriver =
    new G4IntegrationDriver<G4DormandPrince745>(minStep,
						pStepper,
						nvar);
  G4ChordFinder* pChordFinder = new G4ChordFinder(pIntgrationDriver);
  fieldManager->SetChordFinder(pChordFinder);
  */
  return;
}
