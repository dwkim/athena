#include "../FastTrackFinderLRTHypoTool.h"
#include "../FastTrackFinderLRTHypoAlg.h"
#include "../TrigIsoHPtTrackTriggerHypoAlg.h"
#include "../TrigIsoHPtTrackTriggerHypoTool.h"
#include "../TrigdEdxTrackHypoAlg.h"
#include "../TrigdEdxTrackHypoTool.h"
#include "../TrigHitDVHypoAlg.h"
#include "../TrigHitDVHypoTool.h"
#include "../TrigDisappearingTrackHypoAlg.h"
#include "../TrigDisappearingTrackHypoTool.h"

DECLARE_COMPONENT( TrigDisappearingTrackHypoAlg )
DECLARE_COMPONENT( TrigDisappearingTrackHypoTool )
DECLARE_COMPONENT( TrigHitDVHypoAlg )
DECLARE_COMPONENT( TrigHitDVHypoTool )
DECLARE_COMPONENT( TrigdEdxTrackHypoAlg )
DECLARE_COMPONENT( TrigdEdxTrackHypoTool )
DECLARE_COMPONENT( TrigIsoHPtTrackTriggerHypoAlg )
DECLARE_COMPONENT( TrigIsoHPtTrackTriggerHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoAlg )
